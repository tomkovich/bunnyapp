import React from "react";
import NoImg from "./../images/no_user.png";
import { withStyles, Paper } from "@material-ui/core";

const styles = {
  paper: {
    height: 400,
    padding: 15,
    textAlign: "center",
    position: "relative"
  },
  profileDetails: {
    display: "flex",
    flexDirection: "column"
  },
  profileImage: {
    backgroundColor: "#fff",
    width: 100,
    height: 100,
    margin: "10px auto",
    borderRadius: "100%",
    overflow: "hidden",
    border: "3px solid #fff",
    position: "relative"
  },
  profileDetailsItem: {
    display: "flex",
    alignItems: "center",
    backgroundColor: "#c3c3c3",
    margin: "8px 0",
    height: 20,
    width: "50%"
  },
  handle: {
    backgroundColor: "#c3c3c3",
    margin: "8px auto",
    height: 30,
    width: "30%"
  }
};

const ProfileSkeleton = ({ classes }) => {
  const content = (
    <Paper className={classes.paper}>
      <div className={classes.profile}>
        <div className={classes.profileImage}>
          <img src={NoImg} alt="profile" />
        </div>
        <div className={classes.handle}></div>
        <div className={classes.profileDetails}>
            
            <div className={classes.profileDetailsItem}></div>
            <div className={classes.profileDetailsItem}></div>
            <div className={classes.profileDetailsItem}></div>
            <div className={classes.profileDetailsItem}></div>
          </div>
      </div>
    </Paper>
  );

  return content;
};

export default withStyles(styles)(ProfileSkeleton);
