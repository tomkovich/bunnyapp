import React from 'react'
import NoImg from './../images/no_user.png'
import { Card, CardMedia, CardContent, withStyles } from '@material-ui/core'

const styles = {
    card: {
        display: 'flex',
        marginBottom: 20,
        height: 180
    },
    content: {
        width: '100%',
        flexDirection: 'column',
        padding: 25
    },
    cover: {
        minWidth: 110,
        objectFit: 'cover'
    },
    handle: {
        width: 100,
        height: 20,
        backgroundColor: "#c3c3c3",
        marginBottom: 7
    },
    date: {
        width: 50,
        height: 20,
        backgroundColor: "#c3c3c3",
        marginBottom: 20
    },
    fullLine: {
        width: "100%",
        height: 40,
        backgroundColor: "#c3c3c3",
        marginBottom: 15
    },
    halfLine: {
        width: "50%",
        height: 20,
        backgroundColor: "#c3c3c3",
        marginBottom: 40
    }
}

const ScreamSkeleton = ({ classes }) => {

    const content = Array.from({ length: 5 }).map((item, index) => (
        <Card className={classes.card} key={index} >
            <CardMedia className={classes.cover} image={NoImg} />
            <CardContent className={classes.content}>
                <div className={classes.handle}></div>
                <div className={classes.date}></div>
                <div className={classes.fullLine}></div>
                <div className={classes.halfLine}></div>
            </CardContent>
        </Card>
    ))

    return content
}

export default withStyles(styles)(ScreamSkeleton)