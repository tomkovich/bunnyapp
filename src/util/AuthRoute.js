import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropsTypes from "prop-types";

const AuthRoute = ({ component: Component, auth, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      auth === true ? <Redirect push to="/" /> : <Component {...props} />
    }
  />
);

let mapStateToProps = state => ({
  auth: state.user.authenticated
});

AuthRoute.propsTypes = {
  user: PropsTypes.object
};

export default connect(mapStateToProps)(AuthRoute);
