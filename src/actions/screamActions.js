import { GET_ALL_SCREAMS } from "../reducers/types";
import axios from "axios";

export const getScreamsAll = () => async dispatch => {
  try {
    let result = await axios.get("/screams");
    dispatch({ type: GET_ALL_SCREAMS, data: result.data });
  } catch (err) {
    console.log(err);
  }
};
