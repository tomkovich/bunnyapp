import {
  LIKE_SCREAM,
  UNLIKE_SCREAM,
  DELETE_SCREAM,
  POST_SCREAM,
  SET_ERRORS,
  CLEAR_ERRORS,
  LOADING_UI,
  STOP_LOADING_UI,
  SET_SCREAM,
  SUBMIT_COMMENT,
  LOADING_DATA,
  GET_ALL_SCREAMS,
  GET_COMMENT,
  SET_COMMENT,
  DELETE_COMMENT
} from "../reducers/types";
import axios from "axios";
import { getScreamsAll } from "./screamActions";

// Like a screams
export const likeScream = screamId => dispatch => {
  axios
    .get(`/scream/${screamId}/like`)
    .then(res => {
      dispatch({ type: LIKE_SCREAM, payload: res.data });
    })
    .catch(err => {
      console.log(err);
    });
};

// Unlike a scream
export const unlikeScream = screamId => dispatch => {
  axios
    .get(`/scream/${screamId}/unlike`)
    .then(res => {
      dispatch({ type: UNLIKE_SCREAM, payload: res.data });
    })
    .catch(err => {
      console.log(err);
    });
};

// Submit comment
export const submitComment = (screamId, commentData) => dispatch => {
  axios
    .post(`/scream/${screamId}/comment`, commentData)
    .then(res => {
      dispatch({ type: SUBMIT_COMMENT, payload: res.data });
      dispatch(getScreamsAll());
      dispatch(getComments(screamId))
      dispatch(clearErrors());
    })
    .catch(err => {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    });
};

// Delete a scream
export const deleteScream = screamId => dispatch => {
  axios
    .delete(`/scream/${screamId}`)
    .then(() => {
      dispatch({ type: DELETE_SCREAM, payload: screamId });
    })
    .catch(err => console.log(err));
};

export const getComments = (screamId) => dispatch => {

  axios
    .get(`/scream/${screamId}/comments`)
    .then(res => {
      dispatch({
        type: GET_COMMENT,
        payload: res.data
      });
    })
    .catch(err => console.log(err));
};

// Delete a comment
export const deleteComment = (commentId, screamId) => dispatch => {
  axios
    .delete(`/comment/${commentId}`)
    .then(() => {
      dispatch({ type: DELETE_COMMENT, payload: commentId });
      dispatch(getScreamsAll());
    })
    .catch(err => console.log(err));
};

// Get a scream
export const getScream = screamId => dispatch => {
  dispatch({ type: LOADING_UI });
  axios
    .get(`/scream/${screamId}`)
    .then(res => {
      dispatch({
        type: SET_SCREAM,
        payload: res.data
      });
      dispatch(getComments(screamId))
      dispatch({ type: STOP_LOADING_UI });
    })
    .catch(err => console.log(err));
};

// Get a scream
export const setComment = (screamId) => dispatch => {
  axios
    .get(`/scream/${screamId}/comments`)
    .then(res => {
      dispatch({
        type: SET_COMMENT
      });
    })
    .catch(err => console.log(err));
};

// Add a scream
export const postScream = newScream => dispatch => {
  dispatch({ type: LOADING_UI });
  axios
    .post(`/scream`, newScream)
    .then(res => {
      dispatch({
        type: POST_SCREAM,
        payload: res.data
      });
      dispatch(clearErrors());
    })
    .catch(err => {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    });
};

export const getUserData = userHandle => dispatch => {
  dispatch({ type: LOADING_DATA });
  axios
    .get(`/user/${userHandle}`)
    .then(res => {
      dispatch({ type: GET_ALL_SCREAMS, data: res.data.screams });
    })
    .catch(() => {
      dispatch({
        type: SET_ERRORS,
        payload: null
      });
    });
};

export const clearErrors = () => dispatch => {
  dispatch({ type: CLEAR_ERRORS });
};
