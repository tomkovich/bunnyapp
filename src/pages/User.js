import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";

// Actions
import { getUserData } from "./../actions/dataActions";

// Components
import StaticProfile from "../components/Profile/StaticProfile";
import Scream from "../components/Scream/Scream";
import ProfileSkeleton from "./../util/ProfileSkeleton";

// UI
import { Grid } from "@material-ui/core";
import ScreamSkeleton from "../util/ScreamSkeleton";

class User extends Component {
  state = {
    profile: null,
    screamIdParam: null
  };

  componentDidMount() {
    const handle = this.props.match.params.handle;
    const screamId = this.props.match.params.screamId;

    if(screamId) this.setState({ screamIdParam: screamId })

    this.props.getUserData(handle);
    axios
      .get(`/user/${handle}`)
      .then(res => {
        this.setState({
          profile: res.data.user
        });
      })
      .catch(err => console.log(err));
  }

  render() {
      
    const { loading, screams } = this.props.data;
    const { screamIdParam } = this.state

    let screamsMarkup = loading ? (
      <ScreamSkeleton />
    ) : screams === null ? (
      <p>No screams from this user</p>
    ) : !screamIdParam ? (
      screams.map(scream => <Scream key={scream.screamId} scream={scream} />)
    ) : (
      screams.map(scream => {
        if(scream.screamId !== screamIdParam) {
          return <Scream key={scream.screamId} scream={scream} />
        } else {
          return <Scream key={scream.screamId} scream={scream} openDialog/>
        }
      })
    )

    return (
      <Grid container spacing={3}>
        <Grid item sm={8} xs={12}>
          {screamsMarkup}
        </Grid>
        <Grid item sm={4} xs={12}>
          {this.state.profile === null ? (
            <ProfileSkeleton />
          ) : (
            <StaticProfile profile={this.state.profile} />
          )}
        </Grid>
      </Grid>
    );
  }
}

let mapStateToProps = state => ({
  data: state.data
});

export default connect(mapStateToProps, { getUserData })(withRouter(User));
