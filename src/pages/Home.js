import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Grid } from "@material-ui/core";
import { getScreamsAll } from "./../actions/screamActions";

import Profile from "../components/Profile/Profile";
import Scream from "../components/Scream/Scream";
import ScreamSkeleton from "../util/ScreamSkeleton";

const Home = ({ screams, getScreamsAll, userImage }) => {
  
  useEffect(() => {
    getScreamsAll();
  }, [getScreamsAll, userImage]);

  let screamsRecentMark =
    screams.screams.length > 0 ? (
      screams.screams.map(scream => (
        <Scream key={scream.screamId} scream={scream} />
      ))
    ) : (
      <ScreamSkeleton />
    );

  return (
    <Grid container spacing={3}>
      <Grid item sm={8} xs={12}>
        {screamsRecentMark}
      </Grid>
      <Grid item sm={4} xs={12}>
        <Profile />
      </Grid>
    </Grid>
  );
};

let mapStateToProps = state => ({
  screams: state.data,
  userImage: state.user.credentials.imageUrl
});

let mapDispatchToProps = {
  getScreamsAll
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
