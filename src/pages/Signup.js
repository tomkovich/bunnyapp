import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import logo from "./../images/bunny.svg";

// MUI Stuff
import { Typography } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import withStyles from "@material-ui/core/styles/withStyles";
import { Grid } from "@material-ui/core";

// Redux
import { connect } from "react-redux";
import { signupUser } from "../actions/userActions";

const styles = {
  form: {
    textAlign: "center"
  },
  textField: {
    margin: "15px auto"
  },
  button: {
    marginTop: 20,
    position: "relative"
  },
  customError: {
    color: "red"
  },
  linkSignup: {
    width: "100%",
    margin: "10px auto",
    textAlign: "center",
    fontSize: 16,
    marginTop: 25
  },
  progress: {
    position: "absolute",
    width: 20,
    left: -35
  }
};

class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      confirmPassword: "",
      handle: "",
      errors: {}
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.ui.errors !== prevProps.ui.errors) {
      this.setState({
        errors: this.props.ui.errors
      });
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.setState({
      loading: true
    });
    const newUserData = {
      email: this.state.email,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      handle: this.state.handle
    };
    this.props.signupUser(newUserData, this.props.history);
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const {
      classes,
      ui: { loading }
    } = this.props;
    const { errors } = this.state;
    return (
      <Grid container className={classes.form}>
        <Grid item sm></Grid>
        <Grid item sm>
          <img src={logo} alt="bunny" className="logo" />
          <Typography className={classes.pageTitle} variant="h4">
            Signup
          </Typography>
          <form noValidate onSubmit={this.handleSubmit}>
            <TextField
              id="email"
              name="email"
              type="email"
              label="Email"
              className={classes.textField}
              value={this.state.email}
              error={errors.email ? true : false}
              onChange={this.handleChange}
              helperText={errors.email}
              fullWidth
            />
            <TextField
              id="password"
              name="password"
              type="password"
              label="Password"
              className={classes.textField}
              helperText={errors.password}
              error={errors.password ? true : false}
              value={this.state.password}
              onChange={this.handleChange}
              fullWidth
            />
            <TextField
              id="confirmPassword"
              name="confirmPassword"
              type="password"
              label="Confirm Password"
              className={classes.textField}
              helperText={errors.confirmPassword}
              error={errors.password ? true : false}
              value={this.state.confirmPassword}
              onChange={this.handleChange}
              fullWidth
            />
            <TextField
              id="handle"
              name="handle"
              type="handle"
              label="User Name"
              className={classes.textField}
              helperText={errors.handle}
              error={errors.handle ? true : false}
              value={this.state.handle}
              onChange={this.handleChange}
              fullWidth
            />
            {errors.general && (
              <Typography variant="body2" className={classes.customError}>
                {errors.general}
              </Typography>
            )}
            <Button
              type="submit"
              variant="contained"
              color="secondary"
              className={classes.button}
              disabled={loading}
            >
              Signup
              {loading && (
                <CircularProgress
                  size="30"
                  className={classes.progress}
                  color="secondary"
                />
              )}
            </Button>
            <Typography className={classes.linkSignup}>
              Already have an account? <Link to="/login">Log In</Link>
            </Typography>
          </form>
        </Grid>
        <Grid item sm></Grid>
      </Grid>
    );
  }
}

Signup.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  ui: PropTypes.object.isRequired,
  signupUser: PropTypes.func.isRequired
};

let mapStateToProps = state => ({
  user: state.user,
  ui: state.ui
});

export default connect(mapStateToProps, { signupUser })(
  withStyles(styles)(Signup)
);
