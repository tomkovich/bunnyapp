import React, { Component } from "react";
import { Link } from "react-router-dom";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import { Grid } from "@material-ui/core";
import logo from "./../images/bunny.svg";
import { Typography } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";

// redux stuff
import { connect } from "react-redux";
import { loginUser } from "./../actions/userActions";

const styles = {
  form: {
    textAlign: "center"
  },
  textField: {
    margin: "15px auto"
  },
  button: {
    marginTop: 20,
    position: "relative"
  },
  customError: {
    color: "red"
  },
  linkSignup: {
    width: "100%",
    margin: "10px auto",
    textAlign: "center",
    fontSize: 16,
    marginTop: 25
  },
  progress: {
    position: "absolute",
    width: 20,
    left: -35
  }
};

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      errors: {}
    };
  }
  
  componentDidUpdate(prevProps) {
    if(this.props.ui.errors !== prevProps.ui.errors) {
      this.setState({
        errors: this.props.ui.errors
      })
    }
  }
  
  handleSubmit = e => {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(userData, this.props.history);
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const {
      classes,
      ui: { loading }
    } = this.props;
    const { errors } = this.state;
    return (
      <Grid container className={classes.form}>
        <Grid item sm></Grid>
        <Grid item sm>
          <img src={logo} alt="bunny" className="logo" />
          <Typography className={classes.pageTitle} variant="h4">
            Login
          </Typography>
          <form noValidate onSubmit={this.handleSubmit}>
            <TextField
              id="email"
              name="email"
              type="email"
              label="Email"
              className={classes.textField}
              value={this.state.email}
              error={errors.email ? true : false}
              onChange={this.handleChange}
              helperText={errors.email}
              fullWidth
            />
            <TextField
              id="password"
              name="password"
              type="password"
              label="Password"
              className={classes.textField}
              helperText={errors.password}
              error={errors.password ? true : false}
              value={this.state.password}
              onChange={this.handleChange}
              fullWidth
            />
            {errors.general && (
              <Typography variant="body2" className={classes.customError}>
                {errors.general}
              </Typography>
            )}
            <Button
              type="submit"
              variant="contained"
              color="secondary"
              className={classes.button}
              disabled={loading}
            >
              Login
              {loading && (
                <CircularProgress
                  size="30"
                  className={classes.progress}
                  color="secondary"
                />
              )}
            </Button>
            <Typography className={classes.linkSignup}>
              Don't have an account? <Link to="/signup">Sign Up</Link>
            </Typography>
          </form>
        </Grid>
        <Grid item sm></Grid>
      </Grid>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
  loginUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  ui: PropTypes.object.isRequired
};

let mapStateToProps = state => ({
  user: state.user,
  ui: state.ui
});

let mapDispatchToProps = {
  loginUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Login));
