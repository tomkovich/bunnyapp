import React, { useState } from "react";
import { Link } from "react-router-dom";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { connect } from "react-redux";

import FavoriteIcon from "@material-ui/icons/Favorite";
import NotificationsIcon from "@material-ui/icons/Notifications";

import { markNotificationsRead } from "../../actions/userActions";
import ChatIcon from "@material-ui/icons/Chat";
import {
  Badge,
  Tooltip,
  IconButton,
  Menu,
  MenuItem,
  Typography
} from "@material-ui/core";

const Notifications = (props) => {
   const { user: { notifications }, markNotificationsRead } = props

  const [anchor, setAnchor] = useState(null);
  dayjs.extend(relativeTime);
  let notificationIcon;
  let handleOpen = e => {
    setAnchor(e.target);
  };
  let handleClose = () => {
    setAnchor(null);
  };
  let onMenuOpened = () => {
    if (notifications && notifications.length > 0) {
      let unreadNotIds = notifications
        .filter(not => !not.read)
        .map(not => not.notificationId);
      markNotificationsRead(unreadNotIds);
    }
  };
  if (notifications && notifications.length > 0) {
    notifications.filter(note => note.read === false).length > 0
      ? (notificationIcon = (
          <Badge
            badgeContent={
              notifications.filter(note => note.read === false).length
            }
            color="secondary"
          >
            <NotificationsIcon />
          </Badge>
        ))
      : (notificationIcon = <NotificationsIcon />);
  } else {
    notificationIcon = <NotificationsIcon />;
  }

  let notificationsMarkup =
    notifications && notifications.length > 0 ? (
      notifications.map(not => {
        const verb = not.type === "like" ? "liked" : "commented on";
        const time = dayjs(not.createdAt).fromNow();
        const iconColor = not.read ? "disabled" : "secondary";
        const icon =
          not.type === "like" ? (
            <FavoriteIcon color={iconColor} style={{ marginRight: 10 }} />
          ) : (
            <ChatIcon color={iconColor} style={{ marginRight: 10 }} />
          );

        return (
          <MenuItem key={not.createdAt} onClick={handleClose}>
            {icon}
            <Typography
              component={Link}
              variant="body1"
              to={`/users/${not.recipient}/scream/${not.screamId}`}
            >
              {not.sender} {verb} your Scream {time}
            </Typography>
          </MenuItem>
        );
      })
    ) : (
      <MenuItem onClick={handleClose}>You have no notifications yet</MenuItem>
    );

  return (
    <>
      <Tooltip placement="top" title="Notifications">
        <IconButton
          aria-owns={anchor ? "simple-menu" : undefined}
          aria-haspopup="true"
          onClick={handleOpen}
        >
          {notificationIcon}
        </IconButton>
      </Tooltip>
      <Menu
        anchorEl={anchor}
        open={Boolean(anchor)}
        onClose={handleClose}
        onEntered={onMenuOpened}
      >
        {notificationsMarkup}
      </Menu>
    </>
  );
};

let mapStateToProps = (state) => ({
    user: state.user
});

export default connect(mapStateToProps, { markNotificationsRead })(
  Notifications
);
