import React from "react";
import { connect } from "react-redux"

import LikeButton from "./LikeButton";
import ChatButton from "./ChatButton";


const ScreamRelation = ({ commentCounter, likeCounter, screamId }) => {

  return (
    <>
      <LikeButton screamId={screamId} likeCounter={likeCounter} />
      <ChatButton commentCounter={commentCounter} />
    </>
  );
};

let mapStateToProps = state => ({
    scream: state.data.scream
  });

export default connect(mapStateToProps)(ScreamRelation);
