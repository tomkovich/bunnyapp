import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import { Typography } from "@material-ui/core";
import ChatIcon from "@material-ui/icons/Chat";

import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

import DeleteScream from "./DeleteScream";
import ScreamDialog from "./ScreamDialog";
import LikeButton from "./LikeButton";

const styles = {
  card: {
    display: "flex",
    marginBottom: 20,
    position: "relative"
  },
  image: {
    minWidth: 110,
    objectFit: "cover"
  },
  content: {
    padding: 20
  },
  link: {
    color: "rgb(127, 127, 206)",
    fontWeight: 700
  },
  createdAt: {
    marginBottom: 20
  },
  relation: {
    display: "flex",
    alignItems: "center",
    marginTop: 20,
    "& svg": {
      marginRight: 5,
      cursor: "pointer"
    },
    "& span": {
      margin: "0 15px 0 0"
    }
  }
};

const Scream = props => {
  const {
    classes,
    scream: {
      body,
      createdAt,
      userImage,
      userHandle,
      screamId,
      likeCounter,
      commentCounter
    },
    user: {
      authenticated,
      credentials: { handle }
    }
  } = props;

  const deleteButton =
    authenticated && userHandle === handle ? (
      <DeleteScream screamId={screamId} />
    ) : null;

  dayjs.extend(relativeTime);

  return (
    <Card className={classes.card}>
      <CardMedia
        image={userImage}
        title="Profile Image"
        className={classes.image}
      />
      <CardContent className={classes.content}>
        <Typography
          className={classes.link}
          variant="h5"
          component={Link}
          to={`/users/${userHandle}`}
        >
          {userHandle}
        </Typography>
        {deleteButton}
        <Typography
          className={classes.createdAt}
          variant="body2"
          color="textSecondary"
        >
          {dayjs(createdAt).fromNow()}
        </Typography>
        <Typography variant="body1">{body}</Typography>
        <div className={classes.relation}>
          <LikeButton screamId={screamId} />
          <span>{likeCounter} Likes</span>
          <ChatIcon color="secondary"></ChatIcon>
          <span>{commentCounter} comments</span>
          <ScreamDialog
            likeCounter={likeCounter}
            commentCounter={commentCounter}
            screamId={screamId}
            userHandle={userHandle}
            openDialog={props.openDialog}
          />
        </div>
      </CardContent>
    </Card>
  );
};

let mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps)(withStyles(styles)(Scream));
