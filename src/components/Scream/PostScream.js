import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/styles";
import { connect } from "react-redux";
import { Paper, TextField, Button, Grid } from "@material-ui/core";
import { postScream, clearErrors } from "../../actions/dataActions";
import CircularProgress from "@material-ui/core/CircularProgress";

const styles = {
  root: {
    padding: 15
  },
  formScream: {
    display: "flex",
    flexDirection: "column"
  },
  addButton: {
    marginTop: 20,
    position: "relative",
    marginLeft: "auto"
  },
  progress: {
    position: "absolute",
    width: 20,
    left: -35
  },
  sayBox: {
    display: "flex",
    alignItems: 'center',
    fontWeight: 700,
    justifyContent: "center"
  }
};

const PostScream = ({ classes, postScream, ui, clearErrors }) => {

  const { errors, loading } = ui;
  const [body, setBody] = useState('');
  const [error, setError] = useState(null);

  useEffect(() => {
    if(errors) {
        setError(errors.comment);
    }
    return () => {
      clearErrors();
    };
  }, [errors, clearErrors]);

  let onInputChange = e => {
    setBody(e.target.value);
  };

  let formSubmit = e => {
    e.preventDefault();
    postScream({
      body
    });
    setBody("");
    setError(null)
  };

  return (
    <Grid container spacing={3}>
      <Grid item sm={8} xs={12}>
        <Paper className={classes.root}>
          <form className={classes.formScream} onSubmit={formSubmit}>
            <TextField
              id="bodyScream"
              onChange={onInputChange}
              label="Your Scream"
              className={classes.textField}
              margin="normal"
              multiline
              value={body}
              error={error ? true : false}
              fullWidth
              helperText={error}
            />
            <Button
              type="submit"
              variant="contained"
              color="secondary"
              className={classes.addButton}
              disabled={loading}
            >
              Add scream
              {loading && (
                <CircularProgress
                  size="30"
                  className={classes.progress}
                  color="secondary"
                />
              )}
            </Button>
          </form>
        </Paper>
      </Grid>
      <Grid item sm={4} xs={12} className={classes.sayBox}>
        Hi, what do you want to say?
      </Grid>
    </Grid>
  );
};

let mapStateToProps = state => ({
  ui: state.ui
});

export default connect(mapStateToProps, { postScream, clearErrors })(
  withStyles(styles)(PostScream)
);
