import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { withStyles } from "@material-ui/styles";

import { likeScream, unlikeScream } from "../../actions/dataActions";
import { Tooltip } from "@material-ui/core";

const styles = {};

const LikeButton = ({ user, screamId, likeScream, unlikeScream }) => {
  const { authenticated, likes } = user;

  let likedScream = () => {
    if (likes && likes.find(like => like.screamId === screamId)) {
      return true;
    } else {
      return false;
    }
  };

  let onLikeScream = () => {
    likeScream(screamId);
  };

  let onUnlikeScream = () => {
    unlikeScream(screamId);
  };

  const likeButton = !authenticated ? (
    <Link to="/login">
      <FavoriteBorder color="secondary" />
    </Link>
  ) : likedScream() ? (
    <Tooltip title="Unlike" placement="top">
      <FavoriteIcon color="secondary" onClick={onUnlikeScream} />
    </Tooltip>
  ) : (
    <Tooltip title="Like" placement="top">
      <FavoriteBorder color="secondary" onClick={onLikeScream} />
    </Tooltip>
  );
  return likeButton;
};

let mapStateToProps = state => ({
  user: state.user
});

let mapDispatchToProps = {
  likeScream,
  unlikeScream
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(LikeButton));
