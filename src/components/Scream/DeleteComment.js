import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { Button, Dialog, DialogTitle, DialogActions } from "@material-ui/core";
import DeleteOutline from "@material-ui/icons/DeleteOutline";

import { connect } from "react-redux";

import { deleteComment, getComments } from "./../../actions/dataActions";
import { getScreamsAll } from "./../../actions/screamActions";

const styles = {
  deleteButton: {
    position: "absolute",
    right: 20,
    top: 20,
    cursor: "pointer"
  }
};

class DeleteComment extends Component {
  state = {
    open: false
  };

  handleOpen = () => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({
      open: false
    });
  };

  deleteComment = () => {
    this.props.deleteComment(this.props.commentId, this.props.screamId);
    this.setState({
      open: false
    }); 
  };

  componetDidMount(){
    this.props.getScreamsAll()
  }

  render() {
    const { classes } = this.props;
    
    return (
      <div>
        <DeleteOutline
          onClick={this.handleOpen}
          className={classes.deleteButton}
        />
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <DialogTitle>
            Are you sure you want to delete this comment?
          </DialogTitle>
          <DialogActions>
            <Button onClick={this.handleClose} color="default">
              Cancel
            </Button>
            <Button onClick={this.deleteComment} color="secondary">
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default connect(null, { deleteComment, getScreamsAll, getComments })(
  withStyles(styles)(DeleteComment)
);
