import React from "react";
import { withStyles } from "@material-ui/styles";
import { Grid, Typography } from "@material-ui/core";
import dayjs from "dayjs";
import DeleteComment from "./DeleteComment";

const styles = {
  wrappper: {
    position: "relative",
    padding: 20
  },
  comment: {
    margin: "15px 0",
    padding: "15px 0 0 0",
    borderTop: "1px solid #d6d6d6",
    position: "relative"
  },
  commentData: {
    marginLeft: 20,
    fontSize: 14
  },
  imageProfile: {
    maxWidth: 100,
    height: 85,
    overflow: "hidden",
    objectFit: "cover"
  },
  userHandle: {
    color: "rgb(127, 127, 206)",
    fontWeight: 700
  },
  date: {
    color: "#555",
    margin: "5px 0 12px 0"
  },
  progress: {
    position: "absolute",
    width: 90,
    left: "40%",
    top: 30
  }
};

const Comments = ({
  comments,
  classes,
  authenticated,
  handle,
  allComments
}) => {
  return (
    <Grid container>
      {comments.map((comment, index) => (
          <Comment
            key={index}
            handle={handle}
            comment={comment}
            classes={classes}
            authenticated={authenticated}
          />
        ))}
    </Grid>
  );
};

const Comment = ({ comment, classes, authenticated, handle }) => {
  const {
    body,
    createdAt,
    userImage,
    userHandle,
    commentId,
    screamId
  } = comment;

  const deleteButton =
    authenticated && userHandle === handle ? (
      <DeleteComment commentId={commentId} screamId={screamId} />
    ) : null;

  return (
    <>
      <Grid item sm={12} className={classes.comment}>
        <Grid container>
          <Grid item sm={2}>
            <div className={classes.imageProfile}>
              <img src={userImage} alt="User" />
            </div>
          </Grid>
          <Grid item sm={9}>
            <div className={classes.commentData}>
              <a href={`/users/${userHandle}`} className={classes.userHandle}>
                {userHandle}
              </a>
              <Typography variant="body2" className={classes.date}>
                {dayjs(createdAt).format("h:mm a, MMMM DD YYYY")}
              </Typography>
              <Typography variant="body1" className={classes.body}>
                {body}
              </Typography>
              <DeleteComment />
            </div>
          </Grid>
        </Grid>
        {deleteButton}
      </Grid>
    </>
  );
};

export default withStyles(styles)(Comments);
