import React, { useState, useEffect } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getScream, getComments, setComment } from "./../../actions/dataActions";
import LikeButton from "./LikeButton";
import dayjs from "dayjs";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import { Grid, Typography, Tooltip, IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import CircularProgress from "@material-ui/core/CircularProgress";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import ChatIcon from "@material-ui/icons/Chat";
import Comments from "./Comments";
import CommentForm from "./CommentForm";

const styles = {
  wrappper: {
    position: "relative",
    padding: 20
  },
  closeButton: {
    position: "absolute",
    top: 8,
    right: 8
  },
  imageProfile: {
    maxWidth: 100,
    height: 100,
    overflow: "hidden",
    marginRight: 20
  },
  explandBtn: {
    width: 50,
    height: 50
  },
  moreButton: {
    "& span": {
      margin: "0 !important"
    },
    "& svg": {
      margin: "0 !important"
    }
  },
  userHandle: {
    color: "rgb(127, 127, 206)",
    fontWeight: 700
  },
  date: {
    color: "#555",
    margin: "5px 0 12px 0"
  },
  dialogContent: {
    padding: 15
  },
  progress: {
    position: "absolute",
    width: 90,
    left: "40%",
    top: 30
  },
  spinner: {
    minHeight: 130
  },
  relation: {
    display: "flex",
    alignItems: "center",
    marginTop: 20,
    "& svg": {
      marginRight: 5,
      cursor: "pointer"
    },
    "& span": {
      margin: "5px 15px 0 0"
    }
  }
};

const ScreamDialog = ({
  getComments,
  screamId,
  userHandle,
  scream,
  ui,
  getScream,
  likeCounter, 
  commentCounter,
  classes,
  openDialog,
  user,
  allComments,
  setComment
}) => {
  const { body, createdAt, userImage, comments } = scream;
  const { loading } = ui;
  const {
    authenticated,
    credentials: { handle }
  } = user;

  const initialState = {
    newPath: "",
    oldPath: ""
  };

  const [open, setOpen] = useState(false);
  const [path, setPath] = useState(initialState);

 

  let handleClose = () => {
    window.history.pushState(null, null, path.oldPath);
    setOpen(false);
    setComment(screamId)
  };

  useEffect(() => {
    let handleOpen = () => {
      let oldPath = window.location.pathname;
  
      const newPath = `/users/${userHandle}/scream/${screamId}`;
  
      if (oldPath === newPath) oldPath = `/users/${userHandle}`;
  
      window.history.pushState(null, null, newPath);
  
      setPath({
        newPath,
        oldPath
      });
      setOpen(true);
      getScream(screamId);
    };
    if (openDialog) {
      getComments(screamId);
      handleOpen();
    }
  }, [openDialog, screamId, getComments, getScream]);

  const dialogMarkup = loading ? (
    <div className={classes.spinner}>
      <CircularProgress
        size="100"
        className={classes.progress}
        color="secondary"
      />
    </div>
  ) : (
    <Grid container>
      <Grid item sm={5} className={classes.imageProfile}>
        <img src={userImage} alt="Profile" />
      </Grid>
      <Grid item sm={7}>
        <Typography
          component={Link}
          color="primary"
          variant="h5"
          className={classes.userHandle}
          to={`/users/${userHandle}`}
        >
          @{userHandle}
        </Typography>
        <Typography variant="body2" className={classes.date}>
          {dayjs(createdAt).format("h:mm a, MMMM DD YYYY")}
        </Typography>
        <Typography variant="body1">{body}</Typography>
        <div className={classes.relation}>
          <LikeButton screamId={screamId} />
          <span>{likeCounter} Likes</span>
          <ChatIcon color="secondary"></ChatIcon>
          <span>{commentCounter} comments</span>
        </div>
      </Grid>
      <CommentForm screamId={screamId} getComments={getComments} />
      {allComments && !loading ? (
        <Comments
          comments={allComments}
          handleClose={handleClose}
          handle={handle}
          authenticated={authenticated}
        />
      ) : (
        <CircularProgress
          size="100"
          className={classes.progress}
          color="secondary"
        />
      )}
    </Grid>
  );

  return (
    <>
      <IconButton onClick={handleOpen} className={classes.moreButton}>
        <MoreHorizIcon color="secondary" />
      </IconButton>
      <Dialog
        open={open}
        className={classes.wrappper}
        onClose={handleClose}
        fullWidth
        maxWidth="sm"
      >
        <Tooltip title="Close" placement="top" className={classes.closeButton}>
          <IconButton onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        </Tooltip>
        <DialogContent className={classes.dialogContent}>
          {dialogMarkup}
        </DialogContent>
      </Dialog>
    </>
  );
};

let mapStateToProps = state => ({
  ui: state.ui,
  scream: state.data.scream,
  user: state.user,
  allComments: state.data.allComments
});

export default connect(mapStateToProps, { getScream, getComments, setComment })(
  withStyles(styles)(ScreamDialog)
);
