import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/styles";
import { Button, Grid, TextField } from "@material-ui/core";
import { connect } from "react-redux";
import { submitComment, clearErrors, getScream } from "./../../actions/dataActions";

const styles = {
  commentForm: {
    display: "flex",
    flexDirection: "column",
    margin: "25px 0"
  },
  addButton: {
    marginTop: 20,
    position: "relative",
    marginLeft: "auto"
  }
};

const CommentForm = ({ classes, screamId, auth, ui, submitComment, clearErrors, getScream, getComments }) => {
  const [body, setBody] = useState("");
  const [error, setError] = useState(null);
  const { errors } = ui;

  useEffect(() => {
    if (errors) {
      setError(errors.comment);
      return () => {
        clearErrors();
      };
    }
  }, [errors, clearErrors]);

  let onInputChange = e => {
    setBody(e.target.value);
  };

  let handleSubmit = e => {
    e.preventDefault();
    submitComment(screamId, { body });
    setBody("");
    setError(null);
  };

  let commentFormMarkup = auth ? (
    <Grid item sm={12} style={{ textAlign: "center" }}>
      <form className={classes.commentForm} onSubmit={handleSubmit}>
        <TextField
          name="body"
          type="text"
          label="Your comment"
          value={body}
          fullWidth
          helperText={error}
          onChange={onInputChange}
          error={error ? true : false}
        />
        <Button
          type="submit"
          variant="contained"
          color="secondary"
          className={classes.addButton}
        >
          Add Comment
        </Button>
      </form>
    </Grid>
  ) : null;

  return commentFormMarkup;
};

let mapStateToProps = state => ({
  ui: state.ui,
  auth: state.user.authenticated
});

export default connect(mapStateToProps, { submitComment, clearErrors, getScream })(
  withStyles(styles)(CommentForm)
);
