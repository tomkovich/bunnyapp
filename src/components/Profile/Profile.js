import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import dayjs from "dayjs";
import ProfileSkeleton from "../../util/ProfileSkeleton";

// Mui Stuff
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import { Paper, Typography, IconButton } from "@material-ui/core";
import MuiLink from "@material-ui/core/Link";
import Tooltip from "@material-ui/core/Tooltip";

// Icons
import LocationOn from "@material-ui/icons/LocationOn";
import LinkIcon from "@material-ui/icons/Link";
import CalendarToday from "@material-ui/icons/CalendarToday";
import InfoIcon from "@material-ui/icons/Info";
import EditIcon from "@material-ui/icons/Edit";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

// Redux
import { connect } from "react-redux";
import { uploadImage, logoutUser } from "./../../actions/userActions";
import EditDetails from "./EditDetails";


const styles = {
  paper: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    padding: 15,
    textAlign: "center",
    position: "relative"
  },
  profileNotFound: {
    color: "#fff"
  },
  profileDetails: {
    display: "flex",
    flexDirection: "column"
  },
  profileImage: {
    backgroundColor: "#fff",
    width: 100,
    height: 100,
    margin: "10px auto",
    borderRadius: "100%",
    overflow: "hidden",
    border: "3px solid #fff",
    position: "relative",
    "& .imageInput": {
      position: "absolute",
      zIndex: 9,
      left: 0,
      width: "100%",
      height: "100%",
      display: "block",
      cursor: "pointer",
      top: 0,
      "& button": {
        width: "100%",
        height: "100%",
        display: "block",
        opacity: 1,
        padding: 0,
        position: "absolute",
        "& svg": {
          color: "#fff",
          opacity: 0,
          top: "50%",
          position: "absolute",
          left: "50%",
          transform: "translate(-50%, -50%)",
          transition: "opacity .25s ease"
        },
        "&:hover svg": {
          opacity: 1
        }
      }
    }
  },
  profileLink: {
    display: "block",
    marginBottom: 10,
    textDecoration: "underline"
  },
  profileDetailsItem: {
    display: "flex",
    alignItems: "center",
    color: "#fff",
    margin: "8px 0"
  },
  profileIcon: {
    marginRight: 15,
    marginBottom: 3,
    width: 20
  },
  buttons: {
    "& a": {
      margin: "15px 5px",
      boxShadow: "none"
    }
  },
  profileWebsite: {
    color: "#fff",
    textDecoration: "underline"
  },
  logout: {
    padding: 0,
    marginRight: 15,
    "&:hover": {
      background: "transparent"
    }
  }
};

class Profile extends Component {
  handleImageChange = e => {
    // send to server
    const formData = new FormData();
    formData.append("image", e.target.files[0], e.target.files[0].name);
    this.props.uploadImage(formData);
  };

  handleEditPicture = () => {
    const fileInput = document.getElementById("imageInput");
    fileInput.click();
  };

  handleLogout = () => {
    this.props.logoutUser();
  };

  render() {
    const {
      classes,
      user: {
        credentials: { handle, createdAt, imageUrl, bio, website, location },
        loading,
        authenticated
      }
    } = this.props;

    let profileMarkup = !loading ? (
      authenticated ? (
        <Paper className={classes.paper}>
          <div className={classes.profile}>
            <div className={classes.profileImage}>
              <img src={imageUrl} alt="profile" />
              <div className="imageInput">
                <input
                  type="file"
                  id="imageInput"
                  hidden="hidden"
                  onChange={this.handleImageChange}
                />
                <Tooltip title="Edit Profile Image" placement="top">
                  <IconButton
                    onClick={this.handleEditPicture}
                    className="button"
                  >
                    <EditIcon />
                  </IconButton>
                </Tooltip>
              </div>
            </div>

            <div className={classes.profileDetails}>
              <MuiLink
                component={Link}
                to={`/users/${handle}`}
                color="primary"
                variant="h5"
                className={classes.profileLink}
              >
                @{handle}
              </MuiLink>

              {bio && (
                <div className={classes.profileDetailsItem}>
                  <InfoIcon color="primary" className={classes.profileIcon} />
                  <Typography variant="body2">{bio}</Typography>
                </div>
              )}

              {location && (
                <div className={classes.profileDetailsItem}>
                  <LocationOn color="primary" className={classes.profileIcon} />
                  <span>{location}</span>
                </div>
              )}
              {website && (
                <div className={classes.profileDetailsItem}>
                  <LinkIcon color="primary" className={classes.profileIcon} />
                  <a
                    href={website}
                    className={classes.profileWebsite}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {" "}
                    {website}
                  </a>
                </div>
              )}
              <div className={classes.profileDetailsItem}>
                <CalendarToday
                  color="primary"
                  className={classes.profileIcon}
                />{" "}
                <span>Joined {dayjs(createdAt).format("MMM YYYY")}</span>
              </div>
              <div className={classes.profileDetailsItem}>
                <Tooltip title="logout" placement="top">
                  <IconButton
                    onClick={this.handleLogout}
                    className={classes.logout}
                  >
                    <ExitToAppIcon
                      color="primary"
                      className={classes.profileIcon}
                    />
                    <Typography variant="body2" color="primary">
                      Logout
                    </Typography>
                  </IconButton>
                </Tooltip>
              </div>
              <EditDetails />
            </div>
          </div>
        </Paper>
      ) : (
        <Paper className={classes.paper + " " + classes.profileNotFound}>
          <Typography variant="body2" align="center">
            Profile not found
          </Typography>
          <div className={classes.buttons}>
            <Button
              variant="contained"
              color="secondary"
              component={Link}
              to="/login"
            >
              Login
            </Button>{" "}
            <Button
              variant="contained"
              color="primary"
              component={Link}
              to="/signup"
            >
              Signup
            </Button>{" "}
          </div>
        </Paper>
      )
    ) : (
      <ProfileSkeleton />
    );

    return profileMarkup;
  }
}

let mapStateToProps = state => ({
  user: state.user
});

let mapDispatchToProps = {
  uploadImage,
  logoutUser
};

Profile.propTypes = {
  user: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Profile));
