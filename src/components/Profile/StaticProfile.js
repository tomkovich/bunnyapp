import React from "react";
import dayjs from "dayjs";
import { Link } from "react-router-dom";

// Mui Stuff
import withStyles from "@material-ui/core/styles/withStyles";
import MuiLink from "@material-ui/core/Link";
import { Paper, Typography } from "@material-ui/core";

// Icons
import LocationOn from "@material-ui/icons/LocationOn";
import LinkIcon from "@material-ui/icons/Link";
import CalendarToday from "@material-ui/icons/CalendarToday";
import InfoIcon from "@material-ui/icons/Info";

const styles = {
  paper: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    padding: 15,
    textAlign: "center",
    position: "relative"
  },
  profileNotFound: {
    color: "#fff"
  },
  profileDetails: {
    display: "flex",
    flexDirection: "column"
  },
  profileImage: {
    backgroundColor: "#fff",
    width: 100,
    height: 100,
    margin: "10px auto",
    borderRadius: "100%",
    overflow: "hidden",
    border: "3px solid #fff",
    position: "relative"
  },
  profileLink: {
    display: "block",
    marginBottom: 10,
    textDecoration: "underline"
  },
  profileDetailsItem: {
    display: "flex",
    alignItems: "center",
    color: "#fff",
    margin: "8px 0"
  },
  profileIcon: {
    marginRight: 15,
    marginBottom: 3,
    width: 20
  },
  profileWebsite: {
    color: "#fff",
    textDecoration: "underline"
  }
};

const StaticProfile = props => {
  
  const {
    classes,
    profile: { handle, createdAt, imageUrl, bio, website, location }
  } = props;

  return (
    <Paper className={classes.paper}>
      <div className={classes.profile}>
        <div className={classes.profileImage}>
          <img src={imageUrl} alt="profile" />
        </div>

        <div className={classes.profileDetails}>
          <MuiLink
            component={Link}
            to={`/users/${handle}`}
            color="primary"
            variant="h5"
            className={classes.profileLink}
          >
            @{handle}
          </MuiLink>

          {bio && (
            <div className={classes.profileDetailsItem}>
              <InfoIcon color="primary" className={classes.profileIcon} />
              <Typography variant="body2">{bio}</Typography>
            </div>
          )}

          {location && (
            <div className={classes.profileDetailsItem}>
              <LocationOn color="primary" className={classes.profileIcon} />
              <span>{location}</span>
            </div>
          )}
          {website && (
            <div className={classes.profileDetailsItem}>
              <LinkIcon color="primary" className={classes.profileIcon} />
              <a
                href={website}
                className={classes.profileWebsite}
                target="_blank"
                rel="noopener noreferrer"
              >
                {" "}
                {website}
              </a>
            </div>
          )}
          <div className={classes.profileDetailsItem}>
            <CalendarToday color="primary" className={classes.profileIcon} />{" "}
            <span>Joined {dayjs(createdAt).format("MMM YYYY")}</span>
          </div>
        </div>
      </div>
    </Paper>
  );
};

export default withStyles(styles)(StaticProfile);
