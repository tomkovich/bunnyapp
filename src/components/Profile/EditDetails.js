import React, { useState } from "react";
import PropTypes from "prop-types";
import { editUserDetails } from "./../../actions/userActions";

import { connect } from "react-redux";
import { Tooltip, IconButton } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import EditIcon from "@material-ui/icons/Edit";

import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
    editBtn: {
        position: 'absolute',
        top: 5,
        right: 5
    }
})

const EditDetails = ({ classes, editUserDetails }) => {

  let initialState = {
    bio: "",
    website: "",
    location: ""
  };

  const [ data, setData ] = useState(initialState);
  const [ open, setOpen ] = useState(false);

  let handleOpen = () => {
    setOpen(true);
  };

  let handleClose = () => {
    setOpen(false);
  };

  let onInputChange = e => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    });
  };

  let formSubmit = () => {
    editUserDetails(data);
    handleClose();
    setData(initialState);
  };

  return (
    <>
      <Tooltip title="Edit details" placement="top" className={classes.editBtn}>
        <IconButton onClick={handleOpen}>
          <EditIcon color="primary" />
        </IconButton>
      </Tooltip>
      <Dialog open={open} onClose={handleClose} fullWidth maxWidth="sm">
        <DialogTitle>Edit your details</DialogTitle>
        <DialogContent>
          <form onSubmit={formSubmit}>
            <TextField
              name="bio"
              type="text"
              label="Bio"
              multiline
              rows="3"
              placeholder="A short bio about yourself"
              className={classes.textField}
              value={data.bio}
              fullWidth
              onChange={onInputChange}
            />
            <TextField
              name="website"
              type="text"
              label="Website"
              placeholder="Your website link"
              className={classes.textField}
              value={data.website}
              fullWidth
              onChange={onInputChange}
            />
            <TextField
              name="location"
              type="text"
              label="Location"
              fullWidth
              placeholder="Where you live?"
              className={classes.textField}
              value={data.location}
              onChange={onInputChange}
            />
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button onClick={formSubmit} color="secondary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};



let mapStateToProps = state => ({
  credentials: state.user.credentials
});

let mapDispatchToProps = {
  editUserDetails
};

EditDetails.propTypes = {
    editUserDetails: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired
  };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(EditDetails));
