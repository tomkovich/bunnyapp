import React from "react";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import red from "@material-ui/core/colors/red";
import jwtDecode from "jwt-decode";

import Navbar from "./components/Layout/Navbar";
import AuthRoute from "./util/AuthRoute";
import "./App.css";

// Pages
import Home from "./pages/Home";
import Login from "./pages/Login";
import Signup from "./pages/Signup";

//Redux
import { Provider } from "react-redux";
import store from "./reducers/store";
import { SET_UNAUTHRENTICATED } from "./reducers/types";
import { getUserData, logoutUser } from "./actions/userActions";
import axios from "axios";
import PostScream from "./components/Scream/PostScream";
import User from "./pages/User";
import Notifications from "./components/Layout/Notifications";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#fff"
    },
    secondary: {
      main: red[600]
    }
  }
});

const token = localStorage.FBIdToken;

if (token) {
  const decodedToken = jwtDecode(token);
  if (decodedToken.exp * 1000 < Date.now()) {
    store.dispatch(logoutUser());
    window.location.href = "/login";
  } else {
    store.dispatch({ type: SET_UNAUTHRENTICATED });
    axios.defaults.headers.common["Authorization"] = token;
    store.dispatch(getUserData());
  }
}

let App = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <div className="App">
          <Router>
            <Navbar />
            <div className="container">
              <Switch>
                <Route exact path="/" render={() => <Home />} />
                <AuthRoute exact path="/login" component={Login} />
                <AuthRoute exact path="/signup" component={Signup} />
                <Route path="/addscream" render={() => <PostScream />} />
                <Route path="/notifications" render={() => <Notifications />} />
                <Route exact path="/users/:handle" render={() => <User />} />
                <Route
                  exact
                  path="/users/:handle/scream/:screamId"
                  render={() => <User />}
                />
              </Switch>
            </div>
          </Router>
        </div>
      </Provider>
    </MuiThemeProvider>
  );
};

export default App;
