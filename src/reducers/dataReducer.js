import {
  GET_ALL_SCREAMS,
  LIKE_SCREAM,
  UNLIKE_SCREAM,
  LOADING_DATA,
  DELETE_SCREAM,
  POST_SCREAM,
  SET_SCREAM,
  SUBMIT_COMMENT,
  GET_COMMENT,
  DELETE_COMMENT,
  SET_COMMENT
} from "./types";

const initialState = {
  screams: [],
  scream: [],
  allComments: [],
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOADING_DATA:
      return {
        ...state,
        loading: true
      };
    case GET_ALL_SCREAMS:
      return {
        ...state,
        loading: false,
        screams: action.data
      };
    case SET_SCREAM:
      return {
        ...state,
        scream: action.payload
      };
    case GET_COMMENT:
      return {
        ...state,
        allComments: action.payload
      };
    case SET_COMMENT:
      return {
        ...state,
        allComments: []
      };
    case LIKE_SCREAM:
    case UNLIKE_SCREAM:
      let index = state.screams.findIndex(
        scream => scream.screamId === action.payload.screamId
      );
      state.screams[index] = action.payload;
      return {
        ...state
      };
    case DELETE_SCREAM:
      return {
        ...state,
        screams: state.screams.filter(
          scream => scream.screamId !== action.payload
        )
      };
    case DELETE_COMMENT:
      let counter = state.scream.commentCounter;
      return {
        ...state,
        allComments: state.allComments.filter(
          comment => comment.commentId !== action.payload
        ),
        scream: {
          ...state.scream,
          commentCounter: counter - 1
        }
      };
    case POST_SCREAM:
      return {
        ...state,
        screams: [action.payload, ...state.screams]
      };
    case SUBMIT_COMMENT:
      let count = state.scream.commentCounter;
      return {
        ...state,
        scream: {
          ...state.scream,
          comments: [action.payload, ...state.scream.comments],
          commentCounter: count + 1
        }
      };
    default:
      return state;
  }
};
