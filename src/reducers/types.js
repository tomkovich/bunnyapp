// user reducer types
export const SET_AUTHRENTICATED = "SET_AUTHRENTICATED";
export const SET_UNAUTHRENTICATED = "SET_UNAUTHRENTICATED";
export const SET_USER = "SET_USER";
export const LOADING_USER = "LOADING_USER";
export const MARK_NOTIFICATIONS = "MARK_NOTIFICATIONS";

// Ui reducer types
export const SET_ERRORS = "SET_ERRORS";
export const LOADING_UI = "LOADING_UI";
export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const STOP_LOADING_UI = "STOP_LOADING_UI";

// Screams reducer types
export const GET_ALL_SCREAMS = "GET_ALL_SCREAMS";
export const SET_SCREAMS = "SET_SCREAMS";
export const SET_SCREAM = "SET_SCREAM";
export const LOADING_DATA = "LOADING_DATA";
export const LIKE_SCREAM = "LIKE_SCREAM";
export const UNLIKE_SCREAM = "UNLIKE_SCREAM";
export const DELETE_SCREAM = "DELETE_SCREAM";
export const POST_SCREAM = "POST_SCREAM";
export const SUBMIT_COMMENT = "SUBMIT_COMMENT";
export const GET_COMMENT = "GET_COMMENT";
export const SET_COMMENT = "SET_COMMENT";
export const DELETE_COMMENT = "DELETE_COMMENT";
