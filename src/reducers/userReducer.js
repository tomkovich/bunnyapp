import {
  SET_USER,
  SET_AUTHRENTICATED,
  SET_UNAUTHRENTICATED,
  LOADING_USER,
  LIKE_SCREAM,
  UNLIKE_SCREAM,
  MARK_NOTIFICATIONS
} from "../reducers/types";

const initialState = {
  authenticated: false,
  credentials: {},
  loading: false,
  likes: [],
  notifications: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_AUTHRENTICATED:
      return {
        ...state,
        authenticated: true
      };
    case SET_UNAUTHRENTICATED:
      return initialState;
    case SET_USER:
      return {
        authenticated: true,
        loading: false,
        ...action.payload
      };
    case LOADING_USER:
      return {
        ...state,
        loading: true
      };
    case LIKE_SCREAM:
      return {
        ...state,
        likes: [
          ...state.likes,
          {
            userHandle: state.credentials.handle,
            screamId: action.payload.screamId
          }
        ]
      };
    case UNLIKE_SCREAM:
      return {
        ...state,
        likes: [
          ...state.likes.filter(
            like => like.screamId !== action.payload.screamId
          )
        ]
      };
    case MARK_NOTIFICATIONS:
      state.notifications.forEach(note => note.read = true)
      return {
        ...state
      }
    default:
      return state;
  }
};
